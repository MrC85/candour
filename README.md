<h2>Candour Code Challenge</h2>

Written in PHP 8.1 & React using Laravel with tailwind & daisyUI

To use:
<ul>
<li>composer install</li>
<li>Set up .env, make sure to add VITE_API_URL ending with /api i.e VITE_API_URL=https://condour.test/api</li>
<li>npm install</li>
<li>php artisan migrate</li>
<li>php artisan db:seed (adds 25 randomised comments)</li>
<li>npm run dev</li>
</ul>
