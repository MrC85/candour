<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommentsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::prefix('/comments')->group(function () {
    Route::post('/', [CommentsController::class, 'all']);
    Route::post('/toggle', [CommentsController::class, 'toggleApproval']);
});
