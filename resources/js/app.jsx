import './bootstrap';

import ReactDOM from 'react-dom/client';
import Comments from "./pages/Comments";

ReactDOM.createRoot(document.getElementById('app')).render(
    <Comments />
);
