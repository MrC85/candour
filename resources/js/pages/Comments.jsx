import React from 'react';
import axios from '../axios';
import { useState, useEffect } from "react";
import ReactMarkdown from "react-markdown";

export default function Comments() {
    const [data, setData] = useState(null);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const [orderBy, setOrderBy] = useState('name');
    const [orderDir, setOrderDir] = useState('asc');
    const [term, setSearchTerm] = useState('');

    useEffect(() => {
        const getData = async () => {
            try {
                const response = await axios.post('/comments', {order_by: orderBy , order_dir: orderDir, term: term});
                setData(response.data[0]);
                setError(null);
            } catch (err) {
                setError(err.message);
                setData(null);
            } finally {
                setLoading(false);
            }
        };
        getData();
    }, [term, orderBy, orderDir]);

    function toggleApproval(e, id) {
        e.preventDefault();
        try {
            const response = axios.post('/comments/toggle', {id: id});
            const newData = data.map((item) => {
                if (item.id === id) {
                    const approved = {
                        ...item,
                        approved: !item.approved,
                    };

                    return approved;
                }

                return item;
            });

            setData(newData);
        } catch (err) {
            setError(err.message);
        }
    }

    return (
        <section className="bg-gray-900 w-1/2 mx-auto">
            {error && (
                <div className="alert alert-error shadow-lg mb-2">
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" className="stroke-current flex-shrink-0 h-6 w-6" fill="none" viewBox="0 0 24 24">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                        <span>{error}</span>
                    </div>
                </div>
            )}
            <div className="flex w-full justify-between mb-2">
                <input type="text" placeholder="Search" className="input input-bordered w-full max-w-xs" onKeyUp={(e) => setSearchTerm(e.target.value)} />
                <div className="flex w-1/4 justify-end space-x-2">
                    <select className="select select-bordered max-w-1/2" onChange={(e) => setOrderBy(e.target.value)}>
                        <option value="name">Name</option>
                        <option value="created">Created</option>
                    </select>
                    <select className="select select-bordered max-w-1/2" onChange={(e) => setOrderDir(e.target.value)}>
                        <option value="asc">ASC</option>
                        <option value="desc">DESC</option>
                    </select>
                </div>
            </div>
            <div className="flex flex-col items-center justify-center mx-auto flex-grow h-full">
                {data && data.map((comment) => (
                        <div className="flex w-full flex-col mb-2 bg-gray-800 rounded p-2" key={comment.id}>
                            <div className="flex w-full justify-between">
                                <h3 className="text-lg m-2">{comment.name}</h3>
                                <a href="#" className="btn btn-ghost" onClick={(e) => toggleApproval(e, comment.id)}>{comment.approved ? 'Disapprove' : 'Approve'}</a>
                            </div>
                            <div><ReactMarkdown children={comment.comment} /></div>
                        </div>
                    ))}
            </div>
        </section>
    );
}
