<?php

namespace Database\Factories;

use App\Models\Comment;
use DavidBadura\FakerMarkdownGenerator\FakerProvider;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    protected $model = Comment::class;

    public function definition()
    {
        $this->faker->addProvider(new FakerProvider($this->faker));

        return [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'comment' => $this->faker->markdown(),
            'approved' => false,
        ];
    }
}
