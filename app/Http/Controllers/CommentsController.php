<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentsRequest;
use App\Http\Requests\ToggleApprovalRequest;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use App\Services\CommentService;

class CommentsController extends Controller
{
    public function __construct(protected CommentService $commentService) {}

    public function all(CommentsRequest $request)
    {
        $valid = $request->safe()->only(['order_by', 'order_dir', 'term']);

        $comments = $this->commentService->getComments($valid);

        return response()->json([
            $comments->map(function(Comment $comment) {
                return new CommentResource($comment);
            })
        ]);
    }

    public function toggleApproval(ToggleApprovalRequest $request)
    {
        $valid = $request->safe()->only(['id']);

        $comment = $this->commentService->toggleComment($valid['id']);

        return response()->json([$comment->approved]);
    }
}
