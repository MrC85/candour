<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'order_by' => 'required|in:name,created',
            'order_dir' => 'required|in:asc,desc',
            'term' => 'sometimes|nullable|string',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
