<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ToggleApprovalRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => 'required|exists:comments,id',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
