<?php

namespace App\Services;

use App\Models\Comment;
use Illuminate\Database\Eloquent\Collection;

class CommentService
{
    /**
     * @param array $valid
     * @return Collection
     */
    public function getComments(array $valid): Collection
    {
        $comments = Comment::query();

        if (array_key_exists('term', $valid) && $valid['term'] !== '') {
            $comments->where('name', 'LIKE', $valid['term'] . '%');
        }

        $comments
            ->orderBy($valid['order_by'] === 'created' ? 'created_at' : 'name', $valid['order_dir']);

        return $comments->get();
    }

    /**
     * @param $id
     * @return Comment
     */
    public function toggleComment($id): Comment
    {
        $comment = Comment::find($id);
        $comment->approved = !$comment->approved;
        $comment->save();

        return $comment;
    }
}
